#include <iostream>
#include <chrono>
#include <ctime>

#include "AppConfig.h"

int main() {

	auto now = std::chrono::system_clock::now();
    std::time_t curr_time = std::chrono::system_clock::to_time_t(now);

	std::cout << "App version: " << APP_VERSION_MAJOR << "."
				<< APP_VERSION_MINOR << std::endl;
	std::cout << "The time is: " <<  std::ctime(&curr_time) << std::endl;

	return 0;
}

